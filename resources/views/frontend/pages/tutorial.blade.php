@extends('frontend.layouts.main')


<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

@section('content')
    <!-- Page Content Wrap -->
    <div class="page_content_wrap page_paddings_yes">
        <div class="content_wrap">
            <!-- Content -->
            <div class="content">
                <!-- Quote Post -->
                <article class="post_item post_item_excerpt odd post">
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="post-quote.html">untuk pemain baru</a>
                        </h3>
                        <div class="post_descr">
                            <blockquote cite="#" class="sc_quote">
                                <p>untuk pemuda yang ingin belajar tenis ataupun mengenal bermain tenis tidak ada salahnya mencoba karna kesalahan dalam setiap perbuatan
                                    maka kami sebagai pelatih adalah tugas kita sebagai membenarkan teknik sampai peraturan dalam bermain yang benar.
                                </p>
                                <p class="sc_quote_title">
                                    <a href="#">Budiono</a>
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </article>
                <!-- /Quote Post -->
                <!-- Video Post -->
                <article class="post_item post_item_excerpt even post">
                    <div class="post_featured">
                        <div class="sc_video_player">
                            {{-- <div class="sc_video_frame sc_video_play_button hover_icon hover_icon_play" data-width="770"
                                data-height="433"
                                data-video="&lt;iframe class=&quot;video_frame&quot; src=&quot;https://player.vimeo.com/video/64433019?autoplay=1&quot; width=&quot;770&quot; height=&quot;433&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
                                <img alt="lesson-5.jpg" src="{{ url('/') }}/frontend/video/video.mp4">
                            </div> --}}
                            <video width="770" height="443" controls>
                                    <source src="{{ url('/') }}/frontend/video/playtenis.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="post-video.html">Video Post</a>
                        </h3>
                        <div class="post_descr">
                            <p>berikut adalah contoh video dalam bermain tenis yang benar
                                dengan posisi badan kesamping kiri dan kaki kiri di depan adalah teknik awal yang benar
                                jika semua teknik sudah benar maka tinggal merasakan titik bola dan titik tengah raket agar mengenai</p>
                        </div>
                    </div>
                </article>
                <!-- /Video Post -->
                <!-- Audio Post -->
                <article class="post_item post_item_excerpt odd post">
                    <div class="post_featured">
                        <div class="sc_audio_player sc_audio sc_audio_info" data-width="" data-height="">
                            <div class="sc_audio_header">
                                <div class="sc_audio_author">
                                    <span class="sc_audio_author_name">Frank Sinatra</span>
                                </div>
                                <h6 class="sc_audio_title">Insert Audio Title Here</h6>
                            </div>
                            <div class="__sc_audio_container">
                                <audio class="__sc_audio" src="media/Dream-Music-Relax.mp3"
                                    data-title="Insert Audio Title Here" data-author="Frank Sinatra"></audio>
                            </div>
                        </div>
                    </div>
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="post-audio.html">Audio Post</a>
                        </h3>
                        <div class="post_descr">
                            <p>sehebat apapun seorang maka akan kalah dengan tekat niat dalam berlatih
                                tidak ada kata terlambat dalam belajar ketika memiliki niat yang besar
                                tidak semua orang memahami dalam bermain tenis hanya orang yang benar benar serius menjadikan dia pemain terkenal</p>
                        </div>
                    </div>
                </article>
                <!-- /Audio Post -->
                <!-- Post without image -->
                <article class="post_item post_item_excerpt even post">
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="post-without-image.html">Post Without Image</a>
                        </h3>
                        <div class="post_descr">
                            <p>dalam bermain tenis bukan hanya teknik
                                strategi juga menjadi suatu hal yang sangat penting dalam bermain tenis
                                tidak perlu tubuh yang kekar ataupun badan yang terlalu kurus karna permainan tenis mudah di lakukan oleh siapa saja</p>
                        </div>
                    </div>
                </article>
                <!-- /Post without image -->
                <!-- Gallery Post -->
                <article class="post_item post_item_excerpt odd post">
                    <div class="post_featured">
                        <div id="sc_slider_3"
                            class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_slider_pagination"
                            data-old-width="770" data-old-height="434" data-interval="7977">
                            <div class="slides swiper-wrapper height_434">
                                <div class="swiper-slide slider3_slide1">
                                    <a href="post-gallery.html"></a>
                                </div>
                                <div class="swiper-slide slider3_slide2">
                                    <a href="post-gallery.html"></a>
                                </div>
                                <div class="swiper-slide slider3_slide3">
                                    <a href="post-gallery.html"></a>
                                </div>
                                <div class="swiper-slide slider3_slide4">
                                    <a href="post-gallery.html"></a>
                                </div>
                                <div class="swiper-slide slider3_slide5">
                                    <a href="post-gallery.html"></a>
                                </div>
                            </div>
                            <div class="sc_slider_controls_wrap">
                                <a class="sc_slider_prev" href="#"></a>
                                <a class="sc_slider_next" href="#"></a>
                            </div>
                            <div class="sc_slider_pagination_wrap"></div>
                        </div>
                    </div>
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="post-gallery.html">Gallery Post</a>
                        </h3>
                        <div class="post_descr">
                            <p>berikut adalah foto foto pemain terkenal di indonesia
                                mereka adalah seorang yang awalnya tidak mengerti tenis hingga sampai menjadi pemain terkenal di indonesia
                                bisa jadi anda selanjutnya menjadi penerus dalam menjadi pemain dan membawa nama indonesia</p>
                        </div>
                    </div>
                </article>
                <!-- /Gallery Post -->
                <!-- Link Post -->
                <article class="post_item post_item_excerpt post_format_link even post">
                    <div class="post_content clearfix">
                        <h3 class="post_title">
                            <a href="http://themeforest.net/user/themerex/portfolio">Link Post</a>
                        </h3>
                        <div class="post_descr">
                            <p>
                                <a href="http://themeforest.net/user/themerex/portfolio" title="Go to ThemeREX portfolio"
                                    target="_blank">http://themeforest.net/user/themerex/portfolio</a>
                            </p>
                        </div>
                    </div>
                </article>
                <!-- /Link Post -->
            </div>
            <!-- /Content -->
        </div>
    </div>
    <!-- /Page Content Wrap -->
@endsection


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
